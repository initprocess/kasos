/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "math.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
struct ftow {
	uint16_t l,b;
};
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
char trans_str[64] = {0,};
/**
 * @bref Массив, куда DMA сливает результаты измерений
 * [0] - AC_V
 * [1] - AC_I_DIFF
 * [2] - AC_I
 * [3] - TH1
 * [4] - TH2
 * [5] - THERMO
**/
volatile uint16_t adc[6];

uint8_t rx_buff[BUFSIZE] = {0,}; 	///< буфер приема RS485
uint16_t rx_buff_len;				///< размер принятого MODBUS сообщения
/**
 * @bref Input registers
 */
uint16_t modbus_input_registers[MBINPREGS] = {0,};
/**
 * @bref Holding registers
 */
uint16_t modbus_holding_registers[MBHOLDREGS] = {0,};

uint8_t modbus_response[256];					///< буфер ответа MODBUS
uint16_t modbus_response_len;					///< длина ответа MODBUS

volatile uint16_t voltage_array[VARRSIZE];		///< массив измерений напряжения
volatile uint16_t current_array[VARRSIZE];		///< массив измерений тока
volatile uint16_t voltage_array_pointer = 0;	///< указатель на позицию измерения напряжения, производящееся с данный момент
volatile uint32_t th1_grow = 0;					///< растущая сумма измерений температуры TH1
volatile uint32_t th2_grow = 0;					///< растущая сумма измерений температуры TH2
volatile uint32_t thermo_grow = 0;				///< растущая сумма измерений температуры процессора
volatile uint32_t flow_cnt = 0;					///< растущая литров
volatile uint32_t th1_sum = 0;					///< сумма измерений температуры TH1 за период измерения
volatile uint32_t th2_sum = 0;					///< сумма измерений температуры TH2 за период измерения
volatile uint32_t thermo_sum = 0;				///< сумма измерений температуры THERMO за период измерения
volatile int8_t eom = 0;						///< '1' - конец цикла измерений

uint16_t current_offset = CURRENT_OFFSET;		///< ноль для тока в значении АЦП
uint16_t voltage_offset = VOLTAGE_OFFSET;		///< ноль для напряжения в значении АЦП
struct ftow tmp_struct; ///< структура для перевода float в 2*uint16_t
uint32_t flow_out_cnt = 0;		///< счетчик тиков измерителя потока
uint32_t flow_out_cnt_s = 0;	///< для вычисления скорости потока
uint16_t millisecond = 0;
uint8_t q_second_f = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
/**
 * @brief Расчет контрольной суммы CRC16
 * @param *nData указатель на массив данных
 * @param wLength длина массива
 * @retval сумма CRC16
 */
uint16_t CRC16 (uint8_t *nData, uint16_t wLength)
{
    const uint16_t wCRCTable[] = {
    0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
    0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
    0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
    0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
    0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
    0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
    0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
    0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
    0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
    0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
    0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
    0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
    0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
    0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
    0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
    0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
    0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
    0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
    0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
    0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
    0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
    0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
    0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
    0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
    0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
    0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
    0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
    0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
    0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
    0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
    0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
    0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040 };

    uint8_t nTemp;
    uint16_t wCRCWord = 0xFFFF;

    while (wLength--)
    {
        nTemp = *nData++ ^ wCRCWord;
        wCRCWord >>= 8;
        wCRCWord  ^= wCRCTable[(nTemp & 0xFF)];
    }
    return wCRCWord;
}
/**
 * @brief Формирование ответа на запрос от MODBUS master
 * Основная функция обработки MODBUS запроса. Данные читаются напрямую из RS485 буфера.
 * @param нет
 * @retval нет
 */
void Modbus_response()
{
	uint16_t regs_adr;
	uint16_t regs_cnt;
	uint8_t len;
	uint16_t crc;
	switch (rx_buff[1]) {
	case 3:
		regs_adr = ((rx_buff[2] << 8) | rx_buff[3]);
		regs_cnt = (uint16_t)((rx_buff[4] << 8) | rx_buff[5]);
		if (regs_adr+regs_cnt > (MBHOLDREGS)) {
			Modbus_error(2);
			break;
		}
		modbus_response[0] = MBADDR;
		modbus_response[1] = rx_buff[1];
		modbus_response[2] = regs_cnt*2;
		for (int i=0; i < regs_cnt; i++) {
			modbus_response[i*2+3] = (uint8_t)(modbus_holding_registers[i+regs_adr] >> 8);
			modbus_response[i*2+4] = (uint8_t)(modbus_holding_registers[i+regs_adr] & 0x00FF);
		}
		len = regs_cnt*2+3;
		crc = CRC16(modbus_response, len);
		modbus_response[len] = (uint8_t)(crc & 0x00FF);
		modbus_response[len+1] = (uint8_t)(crc >> 8);
		modbus_response_len = len+2;
		break;
	case 4: ;
		regs_adr = ((rx_buff[2] << 8) | rx_buff[3]);
		regs_cnt = (uint16_t)((rx_buff[4] << 8) | rx_buff[5]);
		if (regs_adr+regs_cnt > (MBINPREGS)) {
			Modbus_error(2);
			break;
		}
		modbus_response[0] = MBADDR;
		modbus_response[1] = rx_buff[1];
		modbus_response[2] = regs_cnt*2;
		for (int i=0; i < regs_cnt; i++) {
			modbus_response[i*2+3] = (uint8_t)(modbus_input_registers[i+regs_adr] >> 8);
			modbus_response[i*2+4] = (uint8_t)(modbus_input_registers[i+regs_adr] & 0x00FF);
		}
		len = regs_cnt*2+3;
		crc = CRC16(modbus_response, len);
		modbus_response[len] = (uint8_t)(crc & 0x00FF);
		modbus_response[len+1] = (uint8_t)(crc >> 8);
		modbus_response_len = len+2;
		break;
	case 6: ;
		uint16_t addr = (uint16_t)((rx_buff[2] << 8) | rx_buff[3]);
		uint16_t data = (uint16_t)((rx_buff[4] << 8) | rx_buff[5]);
		uint8_t err = Modbus_do(addr, data);
		if (err == 0)
		{
			for (int i=0; i < 8; i++) modbus_response[i] = rx_buff[i];
			modbus_response_len = rx_buff_len;
			modbus_holding_registers[addr] = data;
		}
		else if (err == 1) Modbus_error(1);
		break;
	default:
		Modbus_error(1);
	}
	uint8_t res = HAL_UART_Transmit(&huart1, (uint8_t*)modbus_response, modbus_response_len,1000);
	if(res == HAL_ERROR || res == HAL_BUSY) Internal_error(UART_ERROR_HAL);
}
/**
 * @brief Действия при записи в holding registers
 * @param addr адрес регистра
 * @param data данные регистра
 * @retval номер ошибки
 */
uint8_t Modbus_do(uint16_t addr, uint16_t data)
{
	uint8_t return_val = 0;	///< код ошибки при установки регистров 0-все хорошо
	switch (addr)
	{
	case MOD_RELAY:
		if (data == 0) HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, GPIO_PIN_RESET);
		if (data == 1) HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, GPIO_PIN_SET);
		return_val = 0;
		break;
	case MOD_ERRCNT_RESET:
		if (data == 1)
		{
			modbus_input_registers[MOD_ERROR_COUNT] = 0;
			modbus_input_registers[MOD_LAST_ERROR] = 0;
			modbus_holding_registers[MOD_ERRCNT_RESET] = 0;
		}
		return_val = 0;
		break;
	case MOD_FCNT_RESET:
		if (data == 1)
		{
			flow_cnt = 0;
			modbus_input_registers[MOD_FLOW_CNT_L] = 0;
			modbus_input_registers[MOD_FLOW_CNT_B] = 0;
			modbus_holding_registers[MOD_FCNT_RESET] = 0;
		}
		return_val = 0;
	default:
		return_val = 1;
	}
	return return_val;
}
/**
 * @brief Формирование пакета ошибки MODBUS
 * @param err_num номер ошибки
 * @retval нет
 */
void Modbus_error(int err_num)
{
	uint16_t crc;
	switch (err_num)
	{
	case 1:
		modbus_response[0] = MBADDR;
		modbus_response[1] = rx_buff[1] | 0x80;
		modbus_response[2] = 1;
		crc = CRC16(modbus_response, 3);
		modbus_response[3] = (uint8_t)(crc & 0x00FF);
		modbus_response[4] = (uint8_t)(crc >> 8);
		modbus_response_len = 5;
		break;
	case 2:
		modbus_response[0] = MBADDR;
		modbus_response[1] = rx_buff[1] | 0x80;
		modbus_response[2] = 2;
		crc = CRC16(modbus_response, 3);
		modbus_response[3] = (uint8_t)(crc & 0x00FF);
		modbus_response[4] = (uint8_t)(crc >> 8);
		modbus_response_len = 5;
		break;
	}
}
/**
 * @brief Обработка внутренних ошибок
 * @param hadc err_num номер ошибки
 * @retval нет
 */
void Internal_error(uint16_t err_num)
{
	modbus_input_registers[MOD_LAST_ERROR] = err_num;
	modbus_input_registers[MOD_ERROR_COUNT]++;
}
/**
 * @bref Преобразование float в два modbus регистра
 * */
struct ftow float_to_word_struct(float f) {
	struct ftow ret;
    //uint32_t asInt = *((uint32_t*)&f);
    union float_word
    {
    	float fl;
    	struct
		{
    		uint16_t low;
    		uint16_t high;
		};
    };
    union float_word fl_wd;
    fl_wd.fl = f;
    ret.l = fl_wd.low;
    ret.b = fl_wd.high;
    return ret;
}
/**
 * @brief Прерывание ADC DMA transfer
 * @param hadc ADC handle structure
 * @retval нет
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
    //if(hadc->Instance == ADC1) //check if the interrupt comes from ACD1
    //{
    	/*
    	 * adc[]:
    	 * 0 - AC_V
    	 * 1 - AC_I_DIFF
    	 * 2 - AC_I
    	 * 3 - TH1
    	 * 4 - TH2
    	 * 5 - THERMO
    	**/
    	voltage_array[voltage_array_pointer] = adc[0];
    	current_array[voltage_array_pointer] = adc[2];
    	th1_grow += adc[3];
    	th2_grow += adc[4];
    	thermo_grow += adc[5];
    	voltage_array_pointer++;
    	if (voltage_array_pointer == VARRSIZE) {
    		th1_sum = th1_grow; th1_grow = 0;
    		th2_sum = th2_grow; th2_grow = 0;
    		thermo_sum = thermo_grow; thermo_grow = 0;
    		voltage_array_pointer = 0;
    		eom = 1;
    	}
    //}
}
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
/**
 * Счетчик измерителя потока
 */
	if (GPIO_Pin == FLOW_Pin)
	{
		flow_out_cnt++;
		flow_out_cnt_s++;
	}
}

/**
 * @brief Прерывание, если на шине RS485 кончилась передача. Конец передачи пакета MODBUS.
 * @param *huart UART handle structurе pointer
 * @retval нет
 */
void HAL_UART_IDLE_Callback(UART_HandleTypeDef *huart)
{
	if(huart == &huart1)
	{
		__HAL_UART_DISABLE_IT(&huart1, UART_IT_IDLE);
		rx_buff_len = BUFSIZE - __HAL_DMA_GET_COUNTER(huart->hdmarx);
		if (rx_buff[0] == MBADDR) {
			if ((uint16_t)((rx_buff[rx_buff_len-1] << 8) | rx_buff[rx_buff_len-2]) == CRC16(rx_buff, rx_buff_len-2)) Modbus_response();
			else Internal_error(MOD_ERROR_CRC);
		}
		HAL_UART_AbortReceive(&huart1);
		__HAL_UART_CLEAR_IDLEFLAG(&huart1);
		__HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
		HAL_UART_Receive_DMA(&huart1, (uint8_t*)rx_buff, BUFSIZE);

	}
}
/**
 * @brief Прерывание, если буфер RS485 заполнен.
 * @param *huart UART handle structurе pointer
 * @retval нет
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	  if(huart == &huart1)
	  {
		  __HAL_UART_DISABLE_IT(&huart1, UART_IT_IDLE);
		  //HAL_UART_DMAStop(&huart1);
		  Internal_error(UART_ERROR_BUFFER);
		  HAL_UART_AbortReceive(&huart1);
		  __HAL_UART_CLEAR_IDLEFLAG(&huart1);
		  __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
		  HAL_UART_Receive_DMA(&huart1, (uint8_t*)rx_buff, BUFSIZE);
	  }
}
/**
 * @brief Прерывание по ошибкам RS485 UART.
 * @param *huart UART handle structurе pointer
 * @retval нет
 */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	if(huart == &huart1)
	{
		//__HAL_UART_DISABLE_IT(&huart1, UART_IT_IDLE);
		HAL_UART_DMAStop(&huart1);
		HAL_GPIO_WritePin(USART_DE_GPIO_Port, USART_DE_Pin, GPIO_PIN_RESET);
		uint32_t er = HAL_UART_GetError(&huart1);

		switch(er)
		{
			case HAL_UART_ERROR_PE:
				Internal_error(UART_ERROR_PE);
				__HAL_UART_CLEAR_PEFLAG(&huart1);
				huart->ErrorCode = HAL_UART_ERROR_NONE;
			break;

			case HAL_UART_ERROR_NE:
				Internal_error(UART_ERROR_NE);
				__HAL_UART_CLEAR_NEFLAG(&huart1);
				huart->ErrorCode = HAL_UART_ERROR_NONE;
			break;

			case HAL_UART_ERROR_FE:
				Internal_error(UART_ERROR_FE);
				__HAL_UART_CLEAR_FEFLAG(&huart1);
				huart->ErrorCode = HAL_UART_ERROR_NONE;
			break;

			case HAL_UART_ERROR_ORE:
				Internal_error(UART_ERROR_ORE);
				__HAL_UART_CLEAR_OREFLAG(huart);
				huart->ErrorCode = HAL_UART_ERROR_NONE;
			break;

			case HAL_UART_ERROR_DMA:
				Internal_error(UART_ERROR_DMA);
				huart->ErrorCode = HAL_UART_ERROR_NONE;
			break;

			default:
			break;
		}
	}
}
/**
 * @brief Прерывание по таймеру для мигания светодиода активновси сети :-).
 * @param *htim timer handle structurе pointer
 * @retval нет
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
        if(htim->Instance == TIM14)
        {
        	millisecond++;
        	if(millisecond == 249)
        	{
        		millisecond = 0;
        		q_second_f = 1;
        	}
        }
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  MX_TIM14_Init();
  /* USER CODE BEGIN 2 */
  modbus_input_registers[MOD_FIRMWARE_VER] = FIRMWARE_VERSION;
  HAL_ADCEx_Calibration_Start(&hadc);
  HAL_ADC_Start_DMA(&hadc, (uint32_t*)&adc, 6);
  HAL_TIM_Base_Start(&htim3);
  HAL_TIM_Base_Start_IT(&htim14);
  __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
  HAL_UART_Receive_DMA(&huart1, (uint8_t*)rx_buff, BUFSIZE);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  float th_V = 0;
  float th_R = 0;
  float th_T = 0;
  float th_mid = 0;
  uint32_t current = 0;
  uint32_t voltage = 0;
  int32_t cur_temp;
  int32_t vlt_temp;
  uint32_t avg_voltage = 0;
  uint32_t avg_current = 0;
  float voltage_calculated = 0;
  float current_calculated = 0;
  float watt_calculated = 0;
  float watt_inc = 0;

  while (1)
  {
	  /*
	   * Действия при окончании измерений
	   */
	  if (eom == 1)
	  {
		  current = 0;
		  voltage = 0;
		  avg_voltage = 0;
		  avg_current = 0;
		  for (uint16_t i=0; i<VARRSIZE; i++)
		  {
			  cur_temp = current_array[i] - current_offset;
			  current += cur_temp * cur_temp;
			  vlt_temp = voltage_array[i] - voltage_offset;
			  voltage += vlt_temp * vlt_temp;
			  avg_voltage += voltage_array[i];
			  avg_current += current_array[i];
		  }

		  th_mid = th1_sum/VARRSIZE;
		  th_V = VDD_AD/4096*th_mid;
		  th_R = th_V*TH1_R1/(VDD_AD-th_V);
		  th_T = 1.0/(log(th_R/TH1_R1)/TH1_B+1/TH1_T0)-273.16;
		  tmp_struct = float_to_word_struct(th_T);
		  modbus_input_registers[MOD_TH_L] = tmp_struct.l;
		  modbus_input_registers[MOD_TH_B] = tmp_struct.b;

		  th_mid = thermo_sum/VARRSIZE;
		  th_V = VDD_AD/4096*th_mid;
		  th_T = (CPU_T_V25-th_V)/CPU_T_VSLOPE+25.0;
		  tmp_struct = float_to_word_struct(th_T);
		  modbus_input_registers[MOD_THERMO_L] = tmp_struct.l;
		  modbus_input_registers[MOD_THERMO_B] = tmp_struct.b;

		  current_calculated = sqrt(current/VARRSIZE)*ADC_SLOPE*1000/I_R-0.03;
		  tmp_struct = float_to_word_struct(current_calculated);
		  modbus_input_registers[MOD_CURRENT_RMS_L] = tmp_struct.l;
		  modbus_input_registers[MOD_CURRENT_RMS_B] = tmp_struct.b;

		  voltage_calculated = sqrt(voltage/VARRSIZE)*ADC_SLOPE*V_R2_R1;
		  tmp_struct = float_to_word_struct(voltage_calculated);
		  modbus_input_registers[MOD_VOLTAGE_RMS_L] = tmp_struct.l;
		  modbus_input_registers[MOD_VOLTAGE_RMS_B] = tmp_struct.b;

		  watt_calculated = current_calculated*voltage_calculated;
		  tmp_struct = float_to_word_struct(watt_calculated);
		  modbus_input_registers[MOD_WATT_L] = tmp_struct.l;
		  modbus_input_registers[MOD_WATT_B] = tmp_struct.b;

		  watt_inc += watt_calculated/(4.5*VARRSIZE);
		  if (watt_inc > 65535) watt_inc = 0;
		  modbus_input_registers[MOD_WATT_HOURS] = (uint16_t)watt_inc;
		  modbus_input_registers[MOD_V_AVG] = avg_voltage/VARRSIZE;
		  modbus_input_registers[MOD_I_AVG] = avg_current/VARRSIZE;

		  eom = 0;
	  }
	  if (q_second_f == 1) {
		  uint32_t tmp_flow;
		  tmp_flow = flow_out_cnt / FLOW_RATE;
		  modbus_input_registers[MOD_FLOW_CNT_L] = tmp_flow & 0xFFFF;
		  modbus_input_registers[MOD_FLOW_CNT_B] = (tmp_flow >> 16) & 0xFFFF;
		  float tmp_flow_rate;
		  tmp_flow_rate = flow_out_cnt_s*4/(float)FLOW_RATE;
		  flow_out_cnt_s = 0;
		  tmp_struct = float_to_word_struct(tmp_flow_rate);
		  modbus_input_registers[MOD_FLOW_RATE_L] = tmp_struct.l;
		  modbus_input_registers[MOD_FLOW_RATE_B] = tmp_struct.b;
	  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI14|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
